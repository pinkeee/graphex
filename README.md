# Graphex
A small application to turn excel spreadsheets into a radar graph. Written with Pglet.

# About
This project was worked on by a few people in my college class to allow students to login with their 
google account and view their subject results with a nice graph. I will be doing 0 work on this 
it is just for show.

https://github.com/bitsofabyte - Bitsofabyte was one of the devs helping with it

## Example
![All_Centres_Overall](.assets/All_Students_Overall.png)

## Installation
To install the project, make sure you have Python 3.10.0 or above with `python3` in your PATH, then run:
```
python3 setup.py
```

## Dependancies

* kaleido
* pandas
* openpyxl
* plotly
* pglet

**Note:** Please do not use any extra libraries/dependancies unless agreed on                                                                  
**Note:** We may be getting rid of ploty soon after the full development of the new Pglet UI is done

## Running

To run the project, complete the previous section then inside the root project folder run:
```
python src/main.py
```

## Coding standard

This project uses Object-Oriented programming, allowing for it to be easily maintainable in the future and allow for cleaner code.


## Setting up on server

Look at this stuff:

* https://github.com/pglet/pglet/issues/73
* https://pglet.io/docs/pglet-server/installation/
* https://github.com/pglet/pglet/blob/976a6b4ae90db51d25ee81745845641f79a8c9ba/config.template.yml
* https://github.com/pglet/pglet/blob/master/internal/config/config.go#L10-L31
* https://pglet.io/blog

Running the server:
* To run it locally, run:
```
chmod +x pglet
./pglet server
```
* Then run the application in another terminal

What is the config.yml for? Where is the server config?:
* The config.yml in the root of the project folder is for the server config, see configuration settings here: https://github.com/pglet/website/blob/powershell-guide/docs/pglet-server/installation.md

## Diagram of how we are implementing gauth
![diagram](.assets/diagram.png)
