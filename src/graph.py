import pandas as pd
import plotly.graph_objects as go
import plotly.offline as pyo
from pathlib import Path
import os
import sys
import numpy as np 
from pglet import Text, PieChart, VerticalBarChart, LineChart, Stack, LineChart, Image
import pglet.piechart as pieI
import pglet.verticalbarchart as verticalI
import pglet.linechart as lineI

class Graph():
    pngName = ""

    def getStudents(self, file):
        # get all students and return list so we can add it to the combo box
        df = pd.read_csv(file)
        student_list = df['Student'].to_list()
        return student_list

    def getDir(self):
        CURRENT_DIRECTORY = Path(__file__).resolve().parent
        self.pngName = os.fspath(CURRENT_DIRECTORY / (str(np.random.randint(low=100000, high=10000000)) + str(".png")))

        print(self.pngName) 

        return CURRENT_DIRECTORY, self.pngName

    def studentGraph(self, file, student, png=False):
        # Each Header we will plot in the radar graph
        labels = []

        # Reading Excel File and Saving it into a DataFrame
        df = pd.read_csv(file)

        # Looping through the top of the spreadsheet, adding the headers to a list
        for column in df:
            labels.append(column)
        labels.remove('Student')

        student_list = df['Student'].to_list()
        index_no = student_list.index(student)
        selected_row = list(df.loc[index_no])
        selected_row.pop(0)

        labels = [*labels, labels[0]]
        selected_row = [*selected_row, selected_row[0]]

        fig = go.Figure(
            data=[
                go.Scatterpolar(r=selected_row, theta=labels, fill='toself', name=f'{student}')
            ],
            layout=go.Layout(
                title=go.layout.Title(text=f'Filtered By Student: {student}'),
                polar={'radialaxis': {'visible': True}},
                showlegend=True
            )
        )
        fig.update_layout(modebar_remove=['zoom', 'pan', 'select', 'lasso2d','toImage'])
        
        if png:
            fig.write_image(self.getDir()[1])

        pieChart = PieChart(width='100%', legend=True, tooltips=True)
        vBarChart = VerticalBarChart(y_ticks=5, y_min=0, y_max=100, y_format='{y}%', 
            width='100%', height=400, bar_width=10, tooltips=True, legend=True)
        lineChart = LineChart(legend=True, tooltips=True, stroke_width=5, y_min=0, y_max=100, y_format='{y}%', 
                                    x_type='number', width='1000', height='250px')

        line1 = lineI.Data(legend="Student {}".format(student), color='purple')

        chartStack = Stack(horizontal=False, horizontal_align="center", vertical_align='center', controls=[
                pieChart,
                vBarChart,
                lineChart,
                Image(src = self.pngName, title = "Radar Graph", width = 200, height = 200),
        ])

        for i in range(len(labels)):
            pieChart.points.append(pieI.Point(legend=labels[i], value=float(selected_row[i])))
            vBarChart.points.append(verticalI.Point(legend=labels[i], x=labels[i], y=float(selected_row[i]), y_tooltip=(selected_row[i])))
            line1.points.append(lineI.Point(legend=labels[i], x=i, y=float(selected_row[i])))
        lineChart.lines.append(line1)

        return chartStack