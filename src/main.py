import pglet, infoscreen

from selection import *
from graphStage import *

from pglet import Stack, Nav, Dialog
from pglet import nav

class Menu():
    def __init__(self, on_change=None):
        self.on_change = on_change
        self.view = Nav(value="generate", on_change=self.menu_change, items=[
            nav.Item(items=[
                nav.Item(key="generate", text="Generate", icon="Generate"),
                nav.Item(key="info", text="Info", icon="Info"),
                nav.Item(key="signout", text="Sign out", icon="SignOut")
            ])
        ])

    def menu_change(self, e):
        if self.on_change != None:
            self.on_change(self.view.value)
    

# people can change their g account name so this could allow other students to see others data. this would be a breach of GDPR laws. 
# To fix this, we will extact the name from the email, as this cant be changed.
def findName(email):
    # split name from email
    fname = email.split(".")[0]
    sname = email.split(".")[1]

    # make first letters upper
    return str(fname.title() + " " + sname.title())

def main(page):
    print("[INFO] Creating new UI instance, running main.")

    view = None
    stage2Obj = Selection()
    infoscreenObj = infoscreen.InfoScreen()

    def signout(e):
        page.signout()

    def closeDialog(e):
        menu_change("generate")
        menu.value = "generate"
        page.update()

    view = Dialog(open = True, type = "largeHeader", title = "Sign Out", blocking = True, footer = [
        Button(text = "Yes", id = "yes", on_click = signout),
        Button(text = "No", id = "no", on_click = closeDialog)
    ])

    page.padding = 10
    page.vertical_fill = True
    page.title = "Graphex" # set application name, title on webpage
    page.theme = "dark" # try and force dark theme

    screens = {
        "generate": stage2Obj.initStage2(),
        "info": infoscreenObj.view,
        "signout": view
    }

    screen_holder = Stack(width="100%", horizontal_align='center')

    def signin(e):
        page.update()

    def menu_change(screen_name):
        screen_holder.controls.clear()
        screen_holder.controls.append(screens[screen_name])
        page.update()

    menu = Menu(menu_change).view

    layout = Stack(gap=10, horizontal=True, vertical_fill=True, width="100%", controls=[
        Stack(width="230", height="100%", border_radius=5, padding=5, controls=[
            menu
        ]),
        screen_holder
    ])

    page.on_signin = signin
    
    if page.user_login != None:
        # set variables for name and picture        
        stage2Obj.gauthName = findName(page.user_login)
        stage2Obj.gauthEmail = page.user_login

        print("\n[INFO CONNECTION!] Gauth name is: {}\n[INFO CONNECTION!] Gauth email is: {}\n[INFO CONNECTION!] Current ip: {}\n".format(stage2Obj.gauthName, stage2Obj.gauthEmail, page.user_client_ip))

        page.add(layout)
        menu_change("generate")
        page.update()

    return

if __name__ == "__main__":
    pglet.app(name="Graphex", target=main, server="http://localhost:7270", permissions="google:*@accesstomusic.ac.uk") # TODO look at changing permissions to many domains?