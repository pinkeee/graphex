from graph import *
from graphStage import *
from pglet import Text, Tabs, Tab, Button, Textbox, Stack, Image, ChoiceGroup, Spinner, Dropdown, Message, pglet, Toolbar, toolbar, Grid, Column
from pglet import choicegroup, dropdown
import numpy as np

class Selection(StageThree):
    link = "https://docs.google.com/spreadsheets/d/1dk8lATWIZWRkH6CRseUpeO9ACfUbu1JX/gviz/tq?tqx=out:csv"
    gauthName = ""
    gauthEmail = ""
    graphObj = Graph()

    def __init__(self):
        pass

    def initStage2(self):
        # labels
        self.filterLabel = Text(value="Please select options for the type of graph you want to generate", size="large")

        # message 
        self.errorMessage = Message(dismiss=True, visible=False)

        # spinners
        self.loadingSpinnerFilter = Spinner("Generating, please wait...", label_position='top', visible=False, size="large")

        # buttons
        self.generateButton = Button("Generate!", icon="EditCreate", on_click=self.onGenerateClick)

        # main view
        self.stage2Stack = Stack(gap=10, horizontal=True, width="100%", height="100%", controls=[
            Stack(gap=5, width="30%", border_radius=5, padding=5, controls=[
                Stack(height="100%", controls=[
                    Stack(gap=20, horizontal=False, horizontal_align="center", vertical_align='center', width="480", controls=[
                        Stack(horizontal=False, horizontal_align="center", vertical_align='center', controls = [
                            self.errorMessage,
                            Text(value="Selection", size="mega", bold=True),
                            self.filterLabel,
                            self.loadingSpinnerFilter
                        ]),
                        Stack(horizontal=False, horizontal_align="center", vertical_align='center', controls = [
                            self.generateButton
                        ])
                    ])
                ])                                                   
            ]),
            Stack(gap=5, width="70%", border_radius=5, padding=5, controls=[
                StageThree.stage3Stack
            ])
        ])

        return self.stage2Stack

    # def filterChanged(self, e):
    #     if self.filterCg.value == "Filter by student": # show drop down
    #         self.studentDropdown.visible = True
    #         self.secondStudentDropdown.visible = False
    #         self.stage2Stack.update()
    #         return
    #     if self.filterCg.value == "Compare two students":
    #         self.studentDropdown.visible = True
    #         self.secondStudentDropdown.visible = True
    #         self.stage2Stack.update()
    #         return
    #     self.studentDropdown.visible = False
    #     self.secondStudentDropdown.visible = False
    #     self.stage2Stack.update()
    #     return

    def onGenerateClick(self, e):
        StageThree.stage3Stack.controls = [] # fix issue where graphs would just add to other ones

        print("[INFO] Generating graph for {}".format(self.gauthName))

        # if self.filterCg.value == "Filter by student":
        self.loadingSpinnerFilter.visible = True
        self.stage2Stack.update()
            
        self.piechart = self.graphObj.studentGraph(self.link, self.gauthName, True)
        try:
            StageThree.stage3Stack.controls.append(StageThree.graphVal)
            StageThree.stage3Stack.controls.append(self.piechart)
        except AttributeError:
            self.errorMessage.value = "An unknown error has occured while generating graph."
            self.errorMessage.type = "Error"
            self.errorMessage.visible = True
            self.loadingSpinnerFilter.visible = False
            self.stage2Stack.update()

        self.piechart.visible = True

        self.loadingSpinnerFilter.visible = False
        self.stage2Stack.update()
    
        # start to show next stage (showing graph)
        self.errorMessage.value = "Done generating graphs!"
        self.errorMessage.type = "success"
        self.errorMessage.visible = True
        self.stage2Stack.update()

        StageThree.graphVal.value = "Student: {}".format(self.gauthName)
        StageThree.stage3Stack.update()

        return

        # if self.filterCg.value == None:
        #     self.errorMessage.value = "Error, select an option!"
        #     self.errorMessage.type = "error"
        #     self.errorMessage.visible = True
        #     self.loadingSpinnerFilter.visible = False
        #     self.stage2Stack.update()
