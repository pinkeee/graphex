import pglet
import version
from pglet import Stack, Text, Image

class InfoScreen():
    def __init__(self):
        # main view combining all controls
        self.view = Stack(width="100%", min_height="100%", border_radius=5, horizontal_align='center', controls=[
            Stack(padding=10, controls=[
                Stack(width="100%", horizontal_align='center', controls=[
                    Image(src = "https://gitlab.com/pinkeee/graph-logo-host/-/raw/main/logo.png", title = "Graphex", width = 200, height = 200),
                    Text(value = "Graphex", size='xxLarge', bold = True),
                    Text(value = ("Version: {}".format(version.__version__)), size = "large"),
                    Text(value = "Graphex was created with love by the ACC Software Development Y1 team!", size = "large"),
                    Text(value = "www.gitlab.com/pinkeee", markdown = True),
                    Text(value = ("Contact email: {}".format("joseph.todd.302@accesstomusic.ac.uk")), markdown = True),
                    Text(value = "This program comes with absolutely no warranty. Contact the above email for help."),
                    Text(value = "Copyright 2022 - the Graphex team")
                ])
            ])
        ])