from graph import *
from pglet import Text, Tabs, Tab, Button, Textbox, Stack, Image, ChoiceGroup, Spinner, Dropdown, Message, pglet

class StageThree():
    graphVal = Text("Graph", size="mega", bold=True)
    stage3Stack = Stack(gap=10, horizontal=False, horizontal_align="center", vertical_align='center', width="100%", controls=[
        Stack(horizontal=False, horizontal_align="center", vertical_align='center', controls = [
            graphVal
        ])
    ])

    def __init__(self):
        pass
    
    def initStage3(self):
        return self.stage3Stack
