from graph import *
from pglet import Text, Tabs, Tab, Button, Textbox, Stack, Image, ChoiceGroup, Spinner, Dropdown, Message, pglet
from pglet import choicegroup, dropdown

import numpy as np
import regex as re
import os, sys, time

class StageOne():
    link = ""
    sessionNo = 0
    stageObj = None
    graphObj = Graph()
    mainuiObj = None

    def __init__(self, stage2obj, mainui):
        self.stageObj = stage2obj
        self.mainuiObj = mainui
        pass
    
    def initStage1(self):
        # labels
        self.welcomeLabel = Text(value="Welcome!", size="mega", bold=True)
        self.enterLinkLabel = Text(value="Please enter the link to your public sheet here!", size="Large")

        # message 
        self.errorMessage = Message(dismiss=True, visible=False)
        
        # spinners
        self.loadingSpinnerStart = Spinner("Downloading, please wait...", label_position='Top', visible=False, size="large")
        
        # textboxes
        self.sheetPath = Textbox(label='Sheet Link', auto_adjust_height=True, placeholder="Public sheet link here", required=True, prefix="https://") #need auto-adjusted height property

        # buttons
        self.beginButton = Button("Begin!", icon="EditCreate", on_click=self.onBeginClick)

        self.stage1Stack = Stack(gap=20, horizontal=False, horizontal_align="center", vertical_align='center', width="100%", scrollx=False, scrolly=False, controls=[
            Stack(horizontal=False, horizontal_align="center", vertical_align='center', width="480", controls = [
                self.errorMessage,
                self.welcomeLabel,
                self.enterLinkLabel,
                self.loadingSpinnerStart
            ]),
            Stack(horizontal=False, horizontal_align="center", vertical_align='center', width="480", controls = [
                self.sheetPath
            ]),
            Stack(horizontal=False, horizontal_align="center", vertical_align='center', width="480", controls=[
                self.beginButton
            ])
        ])

        return self.stage1Stack



    def onBeginClick(self, e):
        # test link https://docs.google.com/spreadsheets/d/1dk8lATWIZWRkH6CRseUpeO9ACfUbu1JX/edit?usp=sharing&ouid=107728599457541404371&rtpof=true&sd=true
        if self.sheetPath.value == None or not self.sheetPath.value.startswith("https://docs.google.com/spreadsheets/d/"):
            # set error message
            self.errorMessage.value = "Error, please enter a valid link."
            self.errorMessage.type = "Error"
            self.errorMessage.visible = True
            self.stage1Stack.update()

            return

        # format link for the id
        self.link = self.sheetPath.value.removeprefix("https://d")
        tmp = re.sub(r'^.*?/d/', '', self.link)
        sep = '/'
        res = tmp[:tmp.index(sep) + len(sep)]
        if res[33] == "/":
            res=res[:33]+str("") #remove character at index 33

        self.link = "https://docs.google.com/spreadsheets/d/{}/gviz/tq?tqx=out:csv".format(res)
        
        self.sessionNo = int(self.generateSessionNumber())
        self.stageObj.pngName = str(self.sessionNo) + ".png"
        self.stageObj.link = str(self.link)

        # set complete message
        self.errorMessage.value = "Found sheet! Starting generation, time may vary depending on your connection"
        self.errorMessage.type = "success"
        self.errorMessage.visible = True
        self.stage1Stack.update()

        self.loadingSpinnerStart.visible = True
        self.stage1Stack.update()

        # now lets fill the dropdowns for the filter section
        students = self.graphObj.getStudents(self.link)
        self.stageObj.studentDropdown.options = [] # setting options to nothing fixes bug where presses begin again and again would just append all of the students on top of the data already in there.
        self.stageObj.secondStudentDropdown.options = []
        for i in range(len(students)):
            self.stageObj.studentDropdown.options.append(dropdown.Option(students[i]))
            self.stageObj.secondStudentDropdown.options.append(dropdown.Option(students[i]))
            self.stageObj.stage2Stack.update()

        self.loadingSpinnerStart.visible = False
        self.stage1Stack.update()

        self.errorMessage.value = "Generation complete! Proceeding to the next page..."
        self.errorMessage.type = "success"
        self.errorMessage.visible = True
        self.stage1Stack.update()

        time.sleep(1)

        self.mainuiObj.tabVar.value = "Select filters"
        self.mainuiObj.mainTab.update()