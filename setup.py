from shutil import which
from os import system as execute

SUCCESS = '[SUCCESS]'
PROCESSING = '[PROCESSING]'


def setup_pip():
    """
    Make sure pip is installed in the PATH, if it isn't then install it
    """
    print(f"{PROCESSING} Checking 'pip' installation...")
    if which('pip'):
        print(f"{SUCCESS} Installation found in > {which('pip')}")
    else:
        print(f"{PROCESSING} Installing 'pip' with python3...")
        execute('python3 -m pip install -U --force pip')
        print(f"{SUCCESS} 'pip' was installed successfully, continuing setup.")


def setup_requirements():
    """
    Install all requirements from requirements.txt
    """
    print(f"{PROCESSING} Installing requirements...")
    execute('pip install -r requirements.txt')
    print("Finished installing requirements.")

def main():
    """
    Entrypoint for the script
    """
    setup_pip()
    setup_requirements()

if __name__ == "__main__":
    main()
